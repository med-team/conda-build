Source: conda-build
Maintainer: Debian Med Packaging Team <debian-med-packaging@lists.alioth.debian.org>
Uploaders: Andreas Tille <tille@debian.org>
Section: science
Priority: optional
Build-Depends: debhelper-compat (= 12),
               dh-python,
               python3,
               python3-setuptools,
               python3-pytest,
               conda <!nocheck>
Standards-Version: 4.4.1
Vcs-Browser: https://salsa.debian.org/med-team/conda-build
Vcs-Git: https://salsa.debian.org/med-team/conda-build.git
Homepage: https://conda.io/

Package: conda-build
Architecture: all
Depends: ${python3:Depends},
         ${misc:Depends},
         conda
Description: commands and tools for building conda packages
 Conda is a cross-platform, language-agnostic binary package manager. It
 is the package manager used by Anaconda installations, but it may be
 used for other systems as well. Conda makes environments first-class
 citizens, making it easy to create independent environments even for C
 libraries.
 .
 With conda-build it is easy to build own packages for conda, and upload
 them to anaconda.org, a free service for hosting packages for conda, as
 well as other package managers. To build a package, create a recipe. See
 http://github.com/conda/conda-recipes for many example recipes, and
 http://conda.pydata.org/docs/build.html for documentation on how to
 build recipes.
